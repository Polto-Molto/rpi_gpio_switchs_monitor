MAKEFILE      = Makefile

####### Compiler, tools and options

CC            = gcc
CXX           = g++
CFLAGS        = -pipe -O2 -Wall -W -D_REENTRANT -fPIC $(DEFINES)
CXXFLAGS      = -std=c++11 -pipe -O2 -Wall -W -D_REENTRANT -fPIC $(DEFINES)
INCPATH       = -I. -I/usr/local/include -I. 
DEL_FILE      = rm -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p
COPY          = cp -f
COPY_FILE     = cp -f
COPY_DIR      = cp -f -R
INSTALL_FILE  = install -m 644 -p
INSTALL_PROGRAM = install -m 755 -p
INSTALL_DIR   = cp -f -R
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
PKG_CONFIG_PATH = /usr/local/lib/pkgconfig
TAR           = tar -cf
COMPRESS      = gzip -9f
DISTNAME      = sip_caller.1.0.0
DISTDIR = 
LINK          = g++
LFLAGS        = -Wl,-O1 -Wl,-z,origin -Wl,-rpath,\$$ORIGIN
SUBLIBS	      = -L/usr/local/lib
LIBS          = $(SUBLIBS)  -lwiringPi -lpthread 
AR            = ar cqs
RANLIB        = 
SED           = sed
STRIP         = strip

####### Output directory

OBJECTS_DIR   = ./

####### Files
SOURCES       = main.cpp ini.c INIReader.cpp buttonfunction.cpp configfile.cpp
OBJECTS       = main.o   ini.o INIReader.o buttonfunction.o configfile.o
DIST          = define.h main.cpp
QMAKE_TARGET  = oled
DESTDIR       = 
TARGET        = oled


first: all
####### Build rules

$(TARGET):  $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)


all: Makefile $(TARGET)

dist: distdir FORCE
	(cd `dirname $(DISTDIR)` && $(TAR) $(DISTNAME).tar $(DISTNAME) && $(COMPRESS) $(DISTNAME).tar) && $(MOVE) `dirname $(DISTDIR)`/$(DISTNAME).tar.gz . && $(DEL_FILE) -r $(DISTDIR)

distdir: FORCE
	@test -d $(DISTDIR) || mkdir -p $(DISTDIR)
	$(COPY_FILE) --parents $(DIST) $(DISTDIR)/
	$(COPY_FILE) --parents main.cpp $(DISTDIR)/


clean: FORCE 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) $(TARGET)


distclean: clean 
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) Makefile

####### Compile
configfile.o: configfile.cpp configfile.h
buttonfunction.o: buttonfunction.cpp buttonfunction.h
ini.o: ini.c ini.h
INIReader.o: INIReader.cpp INIReader.h
main.o: main.cpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

####### Install

install:  FORCE

uninstall:  FORCE

FORCE:

