#include "configfile.h"

configFile::configFile(string n)
	:_name(n)
{
	  reader = new INIReader(_name);

    if (reader->ParseError() < 0)
        std::cout << "Can't load 'test.ini'\n";
}

int configFile::openFile()
{
	return 0;
}

int configFile::loadData()
{
	return 0;
}

int configFile::buttonNumber(string s)
{
	return reader->GetInteger(s, "num", -1);
}

int configFile::screenTextSize(string s)
{
	return reader->GetInteger(s, "textSize", 1);
}

string configFile::scriptPath(string s)
{
	return reader->Get(s, "script", "UNKNOWN");
}

string configFile::position(string s)
{
	return reader->Get(s, "position", "UNKNOWN");
}

string configFile::screenText(string s)
{
        return reader->Get(s, "text", "UNKNOWN");
}


