#ifndef CONFIG_FILE
#define CONFIG_FILE
#include "defines.h"
#include "INIReader.h"

class configFile
{
public:
	configFile(string);
	int openFile();
	int loadData();

	int buttonNumber(string);
	int screenTextSize(string);
	string scriptPath(string);
	string position(string);
	string screenText(string);
private:
	string _name;
  	INIReader *reader;
};

#endif
