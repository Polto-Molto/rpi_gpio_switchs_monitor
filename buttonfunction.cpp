#include "buttonfunction.h"

       void button_function::setButton(int b)
	{
		_button = b;
	}

        int  button_function::button()
	{
		return _button;
	}

        void button_function::setButtonEnabled()
	{
		button_state = true;
	}

        void button_function::setButtonDisabled()
	{
		button_state = false;
	}

        bool button_function::isButtonEnabled()
	{
		return button_state;
	}

        void button_function::setScreenTextSize(int s)
	{
		screen_text_size = s;
	}

        int  button_function::screenTextSize()
	{
		return screen_text_size;
	}

        void button_function::setScreenText(string s)
	{
		screen_text = s;
	}

        string button_function::screenText()
	{
		return screen_text;
	}

        void button_function::setScreenPosition(string p)
	{
		screen_position = p;
	}

        string button_function::screenPosition()
	{
		return screen_position;
	}


        void button_function::setScriptPath(string p)
        {
                script_path = p;
        }

        string button_function::scriptPath()
        {
                return script_path;
        }

