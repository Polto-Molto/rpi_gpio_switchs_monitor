#ifndef MAIN_DEF
#define MAIN_DEF

#include <cstring>
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <thread>         // std::thread
#include <vector>
#include <sstream>
#include <ctime>

using namespace std;

#endif
