#ifndef BUTTON_FUNCTION
#define BUTTON_FUNCTION

#include "defines.h"

class button_function
{
    public:
	void setButton(int);
	int  button();

	void setButtonEnabled();
	void setButtonDisabled();
	bool isButtonEnabled();

	void setScreenTextSize(int);
	int  screenTextSize();

	void setScreenText(string);
	string screenText();

	void setScreenPosition(string);
	string screenPosition();

	void setScriptPath(string);
	string scriptPath();

    public:
	int _button;
	bool button_state;
	int screen_text_size;
	string screen_text;
	string screen_position;
	string script_path;
};
#endif
