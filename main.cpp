#include "defines.h"
#include <sys/time.h>
#include "configfile.h"
#include "buttonfunction.h"

int count[6] = {0,0,0,0,0,0};
bool state[6] = {true,true,true,true,true,true};

#define IGNORE_CHANGE_BELOW_USEC 100

button_function bf[6];
int status;
struct timeval last_change[6];
int last_msecs[6];
int last_press;
int counter;
int button;

void button_pressed()
{
  struct timeval now;
  unsigned long diff;
  unsigned long msecs;
  unsigned long diff_sec;

  gettimeofday(&now, NULL);
//  diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change[3].tv_sec * 1000000 + last_change[3].tv_usec);
  msecs = (now.tv_sec * 1000 + now.tv_usec/1000);

  diff = msecs - last_msecs[button];
  cout << button << " Diff: " << diff << endl << endl;

  if (diff > IGNORE_CHANGE_BELOW_USEC) {
// Assume that if no bottons have been pressed for a set time (20s) then the display was set to sleep
// After the sleep period, wake the display and ignore button press

    if(digitalRead(bf[button].button()))
    {
// Last time a button was presses is in seconds
      diff_sec = now.tv_sec - last_press;
      cout << "Last Press: " << diff_sec << endl;

      if (diff_sec > 20)
      {
        string cmd1 = string("./oled_show Waking Up 2&");
        system(cmd1.data());
      }
      else
      {
        counter ++;

//        string cmd1 = string("./oled_show ") + to_string(button) + ": " + to_string(counter) +" 2&";
        string cmd1 = string("./oled_show ")+bf[button].screenPosition()+" "+bf[button].screenText()+" "+to_string(bf[button].screenTextSize());
        system(cmd1.data());

        string cmd2 = string(bf[button].scriptPath());
	cout << cmd2 << " :: " << cmd2.length() << endl;
        system(cmd2.data());

        last_msecs[button] = msecs;
      }

      last_press = now.tv_sec;
    }
  }
}

void button_0_pressed()
{
  button = 0;
  button_pressed();
}

void button_1_pressed()
{
  button = 1;
  button_pressed();
}

void button_2_pressed()
{
  button = 2;
  button_pressed();
}

void button_3_pressed()
{
  button = 3;
  button_pressed();
}

void button_4_pressed()
{
  button = 4;
  button_pressed();
}

void button_5_pressed()
{
  button = 5;
  button_pressed();
}


void init(int num,configFile *conf)
{
    last_msecs[num] = 0;
    counter = 0;
    button = 0;
    last_press = 0;
    string button_str = "Button" + to_string(num);
    bf[num].setButton(conf->buttonNumber(button_str));
    bf[num].setScreenTextSize(conf->screenTextSize(button_str));
    bf[num].setScriptPath(conf->scriptPath(button_str));
    bf[num].setScreenPosition(conf->position(button_str));
    bf[num].setScreenText(conf->screenText(button_str));

    pinMode(bf[num].button() , INPUT);
    bf[num].setButtonEnabled();

      cout << "button : " << bf[num].button() << endl;
      cout << "script : " << bf[num].scriptPath() << endl;
      cout << "screen text size : " << bf[num].screenTextSize() << endl;
      cout << "screen text : " << bf[num].screenText() << endl;


}

int main(int argc , char* argv[])
{
  wiringPiSetupGpio();

  char* config_file_name = argv[1];
  configFile *conf = new configFile(config_file_name);
    for(int num = 0 ; num < 6 ; num ++)
	 init(num,conf);

  wiringPiISR (bf[0].button(), INT_EDGE_RISING, &button_0_pressed);
  wiringPiISR (bf[1].button(), INT_EDGE_RISING, &button_1_pressed);
  wiringPiISR (bf[2].button(), INT_EDGE_RISING, &button_2_pressed);
  wiringPiISR (bf[3].button(), INT_EDGE_RISING, &button_3_pressed);
  wiringPiISR (bf[4].button(), INT_EDGE_RISING, &button_4_pressed);
  wiringPiISR (bf[5].button(), INT_EDGE_RISING, &button_5_pressed);

  while(1);
  return 0;
}
